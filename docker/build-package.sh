#!/bin/sh

PATH=/bin:/usr/bin

die() {
    if [ $# -gt 0 ]; then
        printf "%s\n" "$1" >&2
    fi
    exit 1
}

if [ $# -ne 1 ]; then
    printf "usage: %s project_dir\n" "$0" >&2
    exit 2
fi

[ "$(id -u)" != "0" ] || die "must not run as root"

project_dir="$1"
build_dir=~/"$(basename "${project_dir}")"

(
    cd "${build_dir}" && dpkg-buildpackage -us -uc -b -rfakeroot
) || die "debuild failed"
