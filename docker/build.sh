#!/bin/sh

PATH=/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

die() {
    if [ $# -gt 0 ]; then
        printf "%s\n" "$1" >&2
    fi
    exit 1
}

[ "$(id -u)" = "0" ] || die "must run as root"

if [ $# -ne 1 ]; then
    printf "usage: %s project_dir\n" "$0" >&2
    exit 2
fi

project_dir="$1"
builder_uid="$(id -u builder)" || die
builder_gid="$(id -g builder)" || die
builder_home=~builder

# create an isolated network namespace with a loopback interface
ip netns add build || die
ip netns exec build ip link set dev lo up || die

# upgrade packages and install build dependencies
export DEBIAN_FRONTEND=noninteractive
apt update || die "failed to update package metadata"
apt upgrade -y || die "failed to upgrade packages"
mk-build-deps.pl \
    -t 'apt -o Debug::pkgProblemResolver=yes --no-install-recommends -y' \
    -i "${project_dir}/debian/control" || \
    die "failed to install build dependencies"

# directory from which build artifacts will be extracted (GitLab requires this
# to be a subdirectory of the build directory)
mkdir "${project_dir}/debian/result" || die "failed to create result directory"

# prepare source in build directory as builder user
setpriv --clear-groups --reuid "${builder_uid}" --regid "${builder_gid}" \
    --inh-caps=-all --reset-env -- \
    prepare-source.sh "${project_dir}" || die

# run build in isolated network namespace as builder user
ip netns exec build \
    setpriv --clear-groups --reuid "${builder_uid}" --regid "${builder_gid}" \
    --inh-caps=-all --reset-env -- \
    build-package.sh "${project_dir}" || die

# save resulting packages as build artifacts
for pkg in "${builder_home}"/*.deb "${builder_home}"/*.[ud]deb; do
    if [ -f "${pkg}" ]; then
        cp "${pkg}" "${project_dir}/debian/result/" || \
            die "failed to copy package"
    fi
done
