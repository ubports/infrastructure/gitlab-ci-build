#!/bin/sh

PATH=/bin:/usr/bin

die() {
    if [ $# -gt 0 ]; then
        printf "%s\n" "$1" >&2
    fi
    exit 1
}

[ "$(id -u)" != "0" ] || die "must not run as root"

if [ $# -ne 1 ]; then
    printf "usage: %s project_dir\n" "$0" >&2
    exit 2
fi

project_dir="$1"
build_dir=~/"$(basename "${project_dir}")"

cp -rp "${project_dir}" "${build_dir}" || die "failed to copy project"

# download and extract source tarball if debian/ubports.source_location exists
if [ -f "${build_dir}/debian/ubports.source_location" ]; then
    {
        read -r src_url && \
        read -r src_filename
    } < "${build_dir}/debian/ubports.source_location" || \
        die "failed to parse ubports.source_location"
    case ${src_url} in
    http://*|https://*|ftp://*)
        ;;
    *)
        die "invalid url: \"${src_url}\""
        ;;
    esac
    src_filename="$(basename "${src_filename}")"

    wget -O "/tmp/${src_filename}" "${src_url}" || \
        die "failed to download source archive"
    # filter an existing debian subdirectory, strip the outer directory, and
    # extract into $build_dir
    tar -x -v -f "/tmp/${src_filename}" -C "${build_dir}" \
        --strip=1 --transform='s|^[^/]+/debian/||x' --show-transformed || \
        die "failed to extract source archive"
fi
