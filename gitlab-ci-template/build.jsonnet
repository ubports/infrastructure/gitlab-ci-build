function(distro_1, distro_2=null) {
  [if distro != null then "build-package-" + distro else null]: {
    "stage": "build",
    "image": "${REGISTRY_IMAGE_ROOT}/" + distro,
    "script": "/usr/local/bin/build.sh \"${CI_PROJECT_DIR}\"",
    "artifacts": {
      "name": "%s-${CI_COMMIT_REF_SLUG}" % distro,
      "expire_in": "1 day",
      "paths": [
        "debian/result/"
      ]
    }
  }
  for distro in [distro_1, distro_2]
}
