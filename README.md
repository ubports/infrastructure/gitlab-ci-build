GitLab CI Build Project
=======================

This repository contains the Dockerfiles and build scripts used to build
UBports packages using GitLab CI.  It also provides an include file allowing
UBports packages to make use of automated package builds with GitLab CI.
